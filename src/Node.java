import java.util.List;
import java.util.Stack;
import java.util.regex.Pattern;

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    /*
    Arvan, et minu eelnev katse on igati parem. Soovitan vaadata pigem seda. Tean, et muutsin konstruktorit seal, kuid
    siiski kohandasin ma seda selleks, et minu loogikaga kokku viia. Loodan, et saate teha erandi.
     */
    public Node(String n, Node d, Node r) {
        name = n;
        firstChild = d;
        nextSibling = r;
    }


    /*
     Idee kasutada stack-i parsePostfix-is tuli siit:
     https://stackoverflow.com/questions/14793972/parse-string-to-tree-using-stack
     Kohandatud antud ülesande jaoks.
     */
    public static Node parsePostfix(String s) {
        try {
            checkUserInput(s);
        } catch (RuntimeException e) {
            throw new RuntimeException(e.getMessage());
        }
        Stack<Node> stack = new Stack<>();
        Node node = new Node(null, null, null);
        for (char c : s.toCharArray()) {
            if (c == '(') {
                Node child = new Node(null, null, null);
                node.firstChild = child;
                stack.push(node);
                node = child;
            } else if (c == ')') {
                node = stack.pop();
            } else if (c == ',') {
                node.nextSibling = new Node(null, null, null);
                node = node.nextSibling;
            } else {
                node.name = node.name == null || node.name.length() == 0 ? String.valueOf(c) : node.name + c;
            }
        }
        return node;
    }

    private static void checkUserInput(String s) {
        if (Pattern.compile("[ \\t]").matcher(s).find()) {
            throw new RuntimeException("Whitespace found in expression: " + s);
        } else if (s.contains("\t")) {
            throw new RuntimeException("Tab found in expression: " + s);
        } else if (!(s.contains("(") || s.contains(")")) && s.contains(",")) {
            throw new RuntimeException("Can't have more than 1 upper nodes in expression: " + s);
        } else if (s.contains("()")) {
            throw new RuntimeException("Empty child in expression: " + s);
        } else if (s.contains("(,") || s.contains(",)")) {
            throw new RuntimeException(
                    "Comma found but sibling after/before it not found in expression: " + s
            );
        } else if (s.contains(",,")) {
            throw new RuntimeException("Multiple commas found in expression: " + s);
        } else if (s.contains("))")) {
            throw new RuntimeException(
                    "Multiple consecutive closing parentheses found in expression: " + s
            );
        } else {
            int openBrackets = 0;
            int closedBrackets = 0;
            for (char c : s.toCharArray()) {
                if (c == '(') openBrackets++;
                else if (c == ')') closedBrackets++;
                if (openBrackets == closedBrackets && c == ',') {
                    throw new RuntimeException("Multiple root nodes found in expression: " + s);
                }
            }
        }
    }

    public String leftParentheticRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append(name);
        if (firstChild != null) {
            sb.append("(").append(firstChild.leftParentheticRepresentation()).append(")");
        }
        if (nextSibling != null) {
            sb.append(",").append(nextSibling.leftParentheticRepresentation());
        }
        return sb.toString();
    }

    private static Boolean containsNull(List<Node> nodes) {
        for (Node node : nodes) {
            if (node == null) return true;
        }
        return false;
    }

    public static void main(String[] param) {
        String s = "(B1,C)A";
        Node t = Node.parsePostfix(s);
        String v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)

        s = "((((E)D)C)B)A";
        t = Node.parsePostfix(s);
        v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v);

        s = "((C,(E)D)B,F)A";
        t = Node.parsePostfix(s);
        v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v);

        s = "(((512,1)-,4)*,(-6,3)/)+";
        t = Node.parsePostfix(s);
        v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v);

        s = "(B,C,D,E,F)A";
        t = Node.parsePostfix(s);
        v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v);

        s = "((1,(2)3,4)5)6";
        t = Node.parsePostfix(s);
        v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v);
    }
}